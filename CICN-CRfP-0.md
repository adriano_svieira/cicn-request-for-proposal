---
remark: metadados para a ser usado pelo parser de conversão para pdf
date: 31 de outubro de 2014
title: 'CICN Request for Proposal Zero (CRfP-Zero)'
abstract: Esta CRfP conterá a lista de todas as Propostas Requisitadas para o CICN e serão identificadas como CRfP e seguidas de número específico e único.
author:
- affiliation: Dataprev/SUPS
  name: Eng. Adriano dos Santos Vieira
- affiliation: MCTI/LNCC
  name: Prof. Antônio Tadeu
...

\pagebreak

# Centro de Inovação em Computação em Nuvem (CICN)

## Identificação da CRfP

+-------------------------------+----------------------------------------------------------------+
|                               |                                                                |
+===============================+================================================================+
|**CRfP id**:                   | Zero                                                           |
+-------------------------------+----------------------------------------------------------------+
|**Título**:                    |Índice de todas as requisições propostas ao CICN                |
+-------------------------------+----------------------------------------------------------------+
|**Descrição sumária**:         |Esta CRfP conterá a lista de todas as Propostas Requisitadas para o CICN e serão identificadas como CRfP e seguidas de número específico e único.|
+-------------------------------+----------------------------------------------------------------+
|**Situação (status)**:         | em desenvolvimento                                             |
+-------------------------------+----------------------------------------------------------------+
|**Tipo**                       | Informativa                                                    |
+-------------------------------+----------------------------------------------------------------+
|**Autor/Dono**:                | Adriano Vieira <adriano.vieira at dataprev.gov.br>             |
+-------------------------------+----------------------------------------------------------------+
|**Última atualização**:        | 2014-10-31                                                     |
+-------------------------------+----------------------------------------------------------------+
|**Criada em**:                 | 2014-10-31                                                     |
+-------------------------------+----------------------------------------------------------------+

# *CICN Request for Proposal Zero (CRfP-Zero)*

Objetiva agregar idéias de vários contribuidores na definição de **Propostas** a serem submetidas ao CICN. As propostas deverão ser descritas com a maior riqueza de detalhes possível e todos serão bem-vindos para também expandí-las. Cada proposta será nomeada como ***CRfP*** e receberá um estado podendo estar atribuído como **desenvolvimento**, **análise**, **ativa**, **inativa** ou **reprovada**.

Para que possamos organizar as propostas estas terão páginas (ou subpáginas) próprias.

+------+------------------------------------------------+-------------------+------------------------------------------------------------------------------+
| CRfP | Título                                         | Situação (status) | Descrição sumária                                                            |
+======+=========================+======================+===================+==============================================================================+
|Zero  |Índice de todas as requisições propostas ao CICN| desenv            | Esta CRfP conterá a lista de todas as Propostas Requisitadas para o CICN e serão identificadas como CRfP e seguidas de número específico e único|
+------+------------------------------------------------+-------------------+------------------------------------------------------------------------------+
|1     |CRfP Propósito e Linhas de Conduta              | desenv            | Descreve os procedimentos para construção e ciclo de vida de uma CRfP|
+------+------------------------------------------------+-------------------+------------------------------------------------------------------------------+
|2     |CICN Comitê Gestor                              | desenv            | Descreve o **Comitê Gestor do CICN** conforme definido e submetido ao MCTI^[Ministério da Ciência, Tecnologia e Inovação]|
+------+------------------------------------------------+-------------------+------------------------------------------------------------------------------+
|3     |Hadoop para analytics/BI                        | desenv            | Caso de Uso de Hadoop para analytics/BI|
+------+------------------------------------------------+-------------------+------------------------------------------------------------------------------+

Cada um dos requisitos precisarão ser detalhados e terem seus casos de uso (CRfP) adequadamente descritos e com prazos definidos.

## Justificativa

É preciso que se descreva clara e objetivamente as necessidades e demandas de pesquisa e desenvolvimento a serem submetidas ao CICN com vistas a que todos possam visualizar e compreender a demanda submetida e os caminhos do programa CICN.

## Objetivos

Conter uma estrutura básica para de fácil identificação de tópicos e problemas a serem resolvidos ou desaficos a serem alcançados.

## Restrições

Deve haver um espaço para construção colaborativa de cada uma das CRfP facilitando sua construçào e a intereção entre os integrantes do CICN.

## Premissas

O espaço para construção colaborativa deve ser *on line* via internet, incluindo uso de recursos de *"ticktes"* para melhor controle.

## Etapas e cronograma

Para o caso de controle via *ticket* as CRfP devem ser listadas com visão de prazos, como segue:

- Requisições de curto-curtíssimo prazo
- Requisições de médio prazo
- Requisições de longo prazo


